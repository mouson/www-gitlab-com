features:
  primary:
  - name: "Request changes on merge requests"
    available_in: [core, premium, ultimate]  # Include all supported tiers
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#submit-a-review'
    image_url: '/images/16_9/create-request-changes-merge-requests.png'
    reporter: phikai
    stage: create
    categories:
      - 'Code Review Workflow'
    epic_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/11719'
    description: |
      The last part of reviewing a merge request is communicating the outcome. While approving was unambiguous, leaving comments was not. They required the author to read your comments, then determine if the comments were purely informational, or described needed changes. Now, when you complete your review, you can select from three options:

       - **Comment**: Submit general feedback without explicitly approving.
       - **Approve**: Submit feedback and approve the changes.
       - **Request changes**: Submit feedback that should be addressed before merging.

      The sidebar now shows the outcome of your review next to your name. Currently, ending your review with **Request changes** doesn't block the merge request from being merged, but it provides extra context to other participants in the merge request. 

      You can leave feedback about the **Request changes** feature in our [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/438573).
