---
layout: markdown_page
title: "Category Direction - Utilization - Seat Cost Management"
description: "The Utilization strategy page belongs to the Utilization group of the Fulfillment stage"
---

- TOC
{:toc}


## Fulfillment: Utilization - Seat Cost Management Overview

The Utilization group aims to ensure customers have access to seat usage data, so that they can make the optimal decisions for their business needs.

## Vision

_What is our long-term solution concept? Analogy: what will it look like at the top of the mountain?_

Seat usage would provide predictability to our customers through transparent usage visibility, suggested usage management recommendations, and purchasing directions. Our sales teams would be empowered to have customer discussions about growth with detailed usage data as a foundation for that conversation.

## Feature Overview and Maturity

_What features are the Utilization group responsible for and how mature are they?_

**Legend**:

- 🙂 **Minimal**: Available and works for a small number of use cases. Some transparency for internal teams.
- 😊 **Viable**: Available and works for the majority of use cases. Some transparency for internal teams.
- 😁 **Complete**: Fully functional for all eligible use cases. Full transparency for internal teams.
- 😍 **Lovable**: Glowing reviews from external and internal users.

| Category | Feature | Maturity | Description | 
|---------|---------|:--------:|-------------|
|Seat Cost Management (non-add-on products) | Seat Usage Visibility | 😊 Viable | Customers understand how many seats are being used and by whom |
|Seat Cost Management (non-add-on products)| Billable Users Calculation | 🙂 Minimal | Customers understand how many billable seats are being used, by whom, and when they are being used |
|Seat Cost Management (non-add-on products)| Seat Limits | 😊 Viable | Customers understand if they are within the user limits threshold and how to take action (remove, add more, set seat limits) |

## 1-year Plan

_Where are we focused over the next 12 months to make meaningful steps towards achieving our vision and increasing feature maturity?_

### 1-year Vision

In a year from now, we hope to have:
1. Made improvements to [user caps feature](https://gitlab.com/groups/gitlab-org/-/epics/8108)
2. Support further [controls over seat costs](https://gitlab.com/groups/gitlab-org/-/epics/12452) via blocking seat overages

### Roadmap

| What is the work to do? | Why are we doing this work?  | OKR Link  | Part of 1-year vision?  | 
|---------|--------|----|----|
| [User Caps Enhancements](https://gitlab.com/groups/gitlab-org/-/epics/8108) | User caps intends to avoid having billable users > user cap, thus helping GitLab Admins/Owners control the costs of their subscription. Non-billable users don't contribute to billings, so they don't need to be controlled by the user caps feature to accomplish the goal of the feature. | [OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/5817) (Not Public) | Yes |
| Create feature to [block seat overages](https://gitlab.com/groups/gitlab-org/-/epics/12452) | Enables customers to manage their seat usage  | [OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/5818) (Not Public) | Yes |
| Make [user counts consistent](https://gitlab.com/groups/gitlab-org/-/epics/6330) | Our GitLab sales and support team members frequently get reports of User counts in self-managed and SaaS behaving in an unexpected manner and our goal is to improve upon that. | [OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/5819) (Not Public) | Yes |

### Possible Future Opportunities

| What is the work to do? | Why are we considering doing it  |
|---------|--------|
| [Manage roles](https://gitlab.com/gitlab-org/gitlab/-/issues/438068) to prevent role or billable status changes     | Users' roles are easily adjustable, which can have billable implications in some scenarios. Customers want the ability to prevent and/or manage a user's role to ensure it is not adjusting without the correct approvals. |
| [Seat digest](https://gitlab.com/gitlab-org/fulfillment/meta/-/issues/1630) for users with QSRs | These customers are the ones who already get billed quarterly base on the same usage information we would be sending them in these e-mails. They are the ones who could benefit from seeing the digest in advance of the QSR as a way avoid surprise bills. |
| [Dormant User Management for SaaS](https://gitlab.com/groups/gitlab-org/-/epics/7533) | Identifying dormant users and removing them on gitlab.com is a labor intensive and not very intuitive process for our customers. We should make user management, specifically around dormant users, easier so owners can focus their time on less arduous tasks and budgets are not accidentally blown up. |

### Prior Work
- [FY'24 Q1 Fulfillment OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/695) (Not Public)
- [FY'24 Q2 Fulfillment OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1736?iid_path=true) (Not Public)
- [FY'24 Q3 Fulfillment OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3241) (Not Public)
- [FY'24 Q4 Fulfillment OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4469) (Not Public)

## Additional Information

### Q&A

| Question | Answer | 
|---------|-------------|
| What type of customers does Utilization serve? | - SM & SaaS Self-service customers <br>- SM & SaaS Sales assisted customers <br>- SM & SaaS Channel Partners and their customers  |
| What customer personas are Utilization solving for? | Our customers fit the [buyer persona](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/) and may play a different role in the decision-making and purchasing process depending on their company size and their role.   |
| What customer segment is Utilization focused on? | - For SMB and mid-market companies: [The application development manager](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/#app-dev-avery) needs to have visibility into usage across their teams and be able to control usage in a way that fits their company preferences/processes/budget. <br> - For large or enterprise company: [The release and change management director](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/#release-rory) is concerned with accurate billing and being able to make purchasing decisions based on usage information. |
| What internal teams does Utilization serve? | - [Support](https://about.gitlab.com/handbook/support/) <br>- [Customer Success](https://about.gitlab.com/handbook/customer-success/) <br>- [Sales](https://about.gitlab.com/handbook/sales/)  |
