---
layout: markdown_page
title: "Category Direction - Secret Detection"
description: "Secret Detection helps you find and fix leaked secret values like API keys, tokens, and private keys in your GitLab repositories. Learn more about where we're going."
canonical_path: "/direction/secure/static-analysis/secret-detection/"
---

- TOC
{:toc}

## Secret Detection

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2024-01-26` |

### Introduction and how you can help
This direction page describes GitLab's plans for the Secret Detection category, which protects you against leaking credentials, tokens, or other secrets on GitLab.

This page is maintained by the Product Manager for [Static Analysis](/handbook/product/categories/#static-analysis-group), [Connor Gilbert](/company/team/#connorgilbert).

Everyone can contribute to where GitLab Secret Detection goes next, and we'd love to hear from you.
The best ways to participate in the conversation are to:
- Comment or contribute to existing [Secret Detection issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3ASecret%20Detection) in the public `gitlab-org/gitlab` issue tracker.
- If you don't see an issue that matches, file [a new issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20Proposal%20-%20basic).
    - Post a comment that says `@gitlab-bot label ~"group::static analysis" ~"Category:Secret Detection"` so your issue lands in our triage workflow.
- If you're a GitLab customer, discuss your needs with your account team.

### Overview
GitLab Secret Detection helps you avoid a particularly dangerous type of mistake: leaking credentials or other secrets in your code repositories.

We want GitLab to be a safe place to develop software, so we're working to make Secret Detection a standard part of the software development lifecycle (SDLC).
No one should have to _think_ about secrets to be protected from _leaking_ them.

#### The problem
Even experienced developers and teams can slip up and cause serious risk by committing secrets into their code repositories.

The potential damage is significant:
- Secrets often provide access to sensitive data, production systems, or cloud resources that can be abused.
- If a repository is public, automated tools or malicious users can detect and abuse leaked secrets—there are even public sites dedicated to listing these leaks.
- Even if a repository is private within a team or organization, leaked secrets can no longer be trusted to uniquely identify the authorized user(s) in a [non-repudiable](https://en.wikipedia.org/wiki/Non-repudiation) way.

#### GitLab's solution
GitLab Secret Detection helps you prevent the unintentional leak of sensitive information like authentication tokens and private keys.

Secret Detection checks your Git repositories to detect secrets or credentials, then it reports potential findings.
Today, Secret Detection jobs run in your CI/CD pipelines.

We want everyone to be secure, so:
- [Parts of Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/#features-per-tier) are available in every GitLab tier.
- Secret Detection is on by default in [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

In GitLab Ultimate, after you enable Secret Detection:
- You can see and address new Secret Detection findings in [merge requests](https://docs.gitlab.com/ee/user/application_security/#view-security-scan-information-in-merge-requests).
- Results appear in the [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/).
- [Automatic responses](https://docs.gitlab.com/ee/user/application_security/secret_detection/post_processing.html) help mitigate leaks as soon as they occur.

Secret Detection doesn't target a specific language, so you can easily [enable it in any project](https://docs.gitlab.com/ee/user/application_security/secret_detection/).
Our approach takes advantage of [patterns for well-identifiable credentials](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml) like service account keys and API tokens, but also searches for more generic secret types like passwords in certain contexts.

To learn more, check the [Secret Detection documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/).

Outside of the Secret Detection category, GitLab also offers other features that relate to secret values:
- [Push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#prevent-pushing-secrets-to-the-repository) block files with certain names from being pushed to your repositories.
- The [Secrets Management](/direction/verify/secrets_management/) category focuses on enabling GitLab to use and manage secret values.

### Strategy and Themes
<!-- Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

We're specifically focusing on solving the following user problems:
1. **Coverage:** Protecting against credential leaks wherever they occur—in a Git repository, an issue, a job log, or anywhere else.
1. **Time to Remediate:** Reducing the window of time between when a secret is leaked and when it no longer poses a threat.
1. **Prioritization:** Knowing which leaks are the most urgent and serious, so teams can respond to those issues more quickly.

Those problems support two overall goals that are related but distinct:

1. Helping organizations achieve their security and compliance requirements in a unified DevSecOps platform.
    - This goal is primarily focused on organizations using GitLab Ultimate, in accordance with our [tiering philosophy](https://handbook.gitlab.com/handbook/company/pricing/#three-tiers) and [rationale for paid-only features](https://handbook.gitlab.com/handbook/company/stewardship/#what-features-are-paid-only).
1. Making GitLab a safe place for people to develop software.
    - This overall goal applies across tiers, though different features will be available in each tier.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

Our strategic one-year focus is a balance between:
1. Investing in the [next generation of Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/8667), which includes:
    - [Pre-receive scanning](https://gitlab.com/groups/gitlab-org/-/epics/11439), which blocks commits from being pushed if they contain secrets. This functionality is in development, with an early experimental release available in GitLab Dedicated.
    - [Pipelineless post-receive scanning](https://gitlab.com/groups/gitlab-org/-/epics/8626), which replaces the [existing scanning system](https://docs.gitlab.com/ee/user/application_security/secret_detection/) that runs in CI/CD pipelines after content is pushed.
    - [Client-side protections](https://docs.gitlab.com/ee/user/application_security/secret_detection/#warnings-for-potential-leaks-in-text-content), which help users avoid leaking secrets in issues, merge requests, and comments.
1. Addressing problems with how Secret Detection works in its current architecture.

We generally are focusing more on the [next-generation approach](https://gitlab.com/groups/gitlab-org/-/epics/8667), but will still make high-impact improvements to the current architecture.

Specifically, we plan to focus on:

1. Bringing [pre-receive scanning](https://gitlab.com/groups/gitlab-org/-/epics/11439) to broader availability—including specifically to GitLab.com as an opt-in Experiment or Beta release.
    - This contributes to _Coverage_ goals in the _next-generation_ system.
1. [Tracking leaks better](https://gitlab.com/gitlab-org/gitlab/-/issues/387583) as they move through a codebase, to avoid repeatedly surfacing the same leaks.
    - This contributes to _Prioritization_ goals in the _current_ system.
1. Speeding triage by [allowing more exceptions and ruleset customization](https://gitlab.com/groups/gitlab-org/-/epics/10325). This builds on our existing customization features, including the [shared configurations introduced in 16.1](https://about.gitlab.com/releases/2023/06/22/gitlab-16-1-released/#shared-ruleset-customizations-in-sast-iac-scanning-and-secret-detection).
    - This contributes to _Prioritization_ goals in the _current_ system.
1. Scanning codebases [without pipeline jobs](https://gitlab.com/groups/gitlab-org/-/epics/8626), replacing the current pipeline-based approach. This will likely involve rethinking the end-to-end [workflow](https://gitlab.com/gitlab-org/gitlab/-/issues/425994) for detected secrets. This includes providing better organization-wide visibility of possible leaks and better context as leaks are detected.
    - This contributes to _Time to Remediate_ and _Coverage_ goals in the _next-generation_ system.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next 3 months, we are planning to:
- Building, tuning, and performance-testing [pre-receive secret scanning](https://gitlab.com/groups/gitlab-org/-/epics/11439) so that it can be made available on GitLab.com as an [Experiment](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#provide-earlier-access) and then a [Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta). (See the [technical design issue](https://gitlab.com/gitlab-org/gitlab/-/issues/439055) for this milestone.)
    - We plan to target GitLab.com next because it allows real-time monitoring and faster iteration cycles than self-managed release cycles. We will make pre-receive scanning available in all environments as soon as we can.
- A [better tracking mechanism](https://gitlab.com/gitlab-org/gitlab/-/issues/434096) for findings as they move through a codebase. We expect this to make a significant improvement against the [overall problem of repeated/duplicated secret findings](https://gitlab.com/gitlab-org/gitlab/-/issues/387583).

We are also looking forward by:
- [Designing user experience (UX) workflows](https://gitlab.com/gitlab-org/gitlab/-/issues/425994) for pipeline-less post-receive scanning.
- Refining the [system architecture](https://docs.gitlab.com/ee/architecture/blueprints/secret_detection/) for pipelineless post-receive scanning, which should share significant architectural elements with the new pre-receive secret detection feature.

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

We are currently working on:
- Completing the [MVC of pre-receive secret scanning](https://gitlab.com/groups/gitlab-org/-/epics/11587), including [known optimizations and improvements](https://gitlab.com/groups/gitlab-org/-/epics/12287).
- Identifying the [path to enabling pre-receive scanning on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/439055).
- Finalizing [Job to be Done (JTBD) UX researh](https://gitlab.com/gitlab-org/ux-research/-/issues/2707) for Secret Detection.

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

Our recent work includes:

- An initial MVC release of pre-receive secret scanning (16.7). This is currently [available](https://docs.gitlab.com/ee/user/application_security/secret_detection/pre_receive.html) as an Experiment in GitLab Dedicated. We are currently working to expand availability.

Check [older release posts](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features&selectedCategories=Secret+Detection&minVersion=13_00) for our previous work in this area.

#### What is Not Planned Right Now
- **Repositories outside of GitLab:** We don't plan to offer protections for projects hosted outside of GitLab.
- **Credentials not common in software:** We plan to focus on the types of credentials that are most common in DevSecOps workflows and in software development.
This means we **won't** actively pursue rules that are:
    - For services unlikely to be used in a software project.
    - Closer to Data Loss Prevention, for example searching for personally identifiable information (PII) or credit card numbers.

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

_ℹ️ Best In Class (BIC) is an indicator of forecasted near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape._

#### Key Capabilities 
Secret Detection products should:

- Help developers avoid making leaks in the first place.
- Enable quick responses to any leaks that are made.
- Deliver trustworthy, accurate findings, with appropriate priority, so that the right findings are remediated quickly.
- Automatically respond to leaks, without human intervention, when possible.
- Provide security users with overall visibility into exposures anywhere within their organization.

#### Roadmap
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. This may be duplicative to the 1 year section however for some categories the key deliverables required to become the BIC solution will extend beyond one year and we want to capture all of the gaps. Moreover, the 1 year section may contain work that is not directly related to closing gaps if we are already the BIC or if we are differentiating ourselves.-->

The plan [described above](#strategy-and-themes) reinforces our competitive standing.

In addition to those main themes, we will likely pursue additional detection techniques including:
- Checks for whether detected credentials are still active.
- Machine Learning or other solutions for identifying generic or lower-confidence secrets. These secrets, which include passwords and tokens that don't have an identifiable structure, are more difficult to detect while minimizing false-positive results.

#### Top Competitive Solutions
<!-- PMs can choose to highlight a primary BIC competitor--or more, if no single clear winner in the category exists; in this section we should indicate: 1. name of competitive product, 2. links to marketing website and documentation, 3. why we view them as the primary BIC competitor -->

Secret Detection is available in a variety of packaging types:
- Dedicated products like [GitGuardian](https://www.gitguardian.com/) or [TruffleHog Enterprise](https://trufflesecurity.com/trufflehog/).
These products focus on credential leaks, so they typically offer more secret-specific workflows.
- Features in SAST products like [Veracode](https://www.veracode.com/security/static-analysis-tool), [Fortify](https://www.microfocus.com/en-us/cyberres/application-security/static-code-analyzer), or [Snyk](https://docs.snyk.io/scan-application-code/snyk-code/introducing-snyk-code/key-features/ai-engine#hardcoded-secrets).
These products typically add secret detection alongside other types of features, so their workflows are more limited.
- As an integrated part of a platform like GitHub.
These platforms typically offer less advanced workflows, but may offer broader always-on protection.
Platforms differ in the level of scanning they offer for private repositories, the level of customization organizations can apply, and the pricing tier in which such features are available.

We analyze our product against each of these different product types because we serve customers who are accustomed to each of them.
Our approach emphasizes the value of the most comprehensive DevSecOps platform by:
- Protecting the entire DevSecOps lifecycle, including code, issues, and other content.
- Shifting security earlier in the development process, so everyone can contribute to security.

### Target Audience
As with many security categories, Secret Detection is a bridge between different communities:

- Security teams, who are responsible for protecting their organization from credential leaks.
    - These teams often drive the implementation of tools like Secret Detection.
    - They're also often responsible for responding to alerts and incidents related to compromised credentials.
    - User personas include [Alex, the Security Operations Engineer](https://about.gitlab.com/handbook/product/personas/#alex-security-operations-engineer) and [Sam, the Security Analyst](https://about.gitlab.com/handbook/product/personas/#sam-security-analyst).
- Development teams, who are responsible for building software.
    - These teams may accidentally leak credentials in the course of their work.
    - They don't _want_ to be the source of a security breach, but they also don't want to be blocked by unreliable tooling or onerous processes.
    - User personas include [Sasha, the Software Developer](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer), and Sasha's teammates and managers.

### Pricing and Packaging
Secret Detection products vary in how they're provided:
- Some are integrated features within the platforms they protect.
- Others are standalone products.
- Others are modules within other products, like SAST scanners.

GitLab packages and prices Secret Detection primarily as part of Ultimate.
Basic protection features are available in all tiers.
We intend to expand the level of protection available in all tiers, while still delivering unique [organization-level value](https://about.gitlab.com/company/pricing/#three-tiers) in Ultimate.

### Analyst Landscape
Analysts usually include Secret Detection as a secondary feature of Application Security Testing (AST) coverage.
See [Category Direction - Static Application Security Testing (SAST)](../sast/#analyst-landscape) for up-to-date analyst coverage.
