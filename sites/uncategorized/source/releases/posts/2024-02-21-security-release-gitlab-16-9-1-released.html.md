---
title: "GitLab Security Release: 16.9.1, 16.8.3, 16.7.6"
categories: releases
author: Nikhil George
author_gitlab: ngeorge1
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.9.1, 16.8.3, 16.7.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/02/21/security-release-gitlab-16-9-1-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.9.1, 16.8.3, 16.7.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Stored-XSS in user's profile page](#stored-xss-in-users-profile-page) | High |
| [User with "admin_group_members" permission can invite other groups to gain owner access](#user-with-admin_group_members-permission-can-invite-other-groups-to-gain-owner-access) | Medium |
| [ReDoS issue in the Codeowners reference extractor](#redos-issue-in-the-codeowners-reference-extractor) | Medium |
| [LDAP user can reset password using secondary email and login using direct authentication](#ldap-user-can-reset-password-using-secondary-email-and-login-using-direct-authentication) | Medium |
| [Bypassing group ip restriction settings to access environment details of projects through Environments/Operations Dashboard](#bypassing-group-ip-restriction-settings-to-access-environment-details-of-projects-through-environmentsoperations-dashboard) | Medium |
| [Users with the `Guest` role can change `Custom dashboard projects` settings for projects in the victim group](#users-with-the-guest-role-can-change-custom-dashboard-projects-settings-for-projects-in-the-victim-group) | Medium |
| [Group member with sub-maintainer role can change title of shared private deploy keys](#group-member-with-sub-maintainer-role-can-change-title-of-shared-private-deploy-keys) | Low |
| [Bypassing approvals of CODEOWNERS](#bypassing-approvals-of-codeowners) | Low |

### Stored-XSS in user's profile page

An issue has been discovered in GitLab CE/EE affecting version 16.9 only. A crafted payload added to the user profile page could lead to a stored XSS on the client side which allows attackers to perform arbitrary actions on behalf of victims. This is a high severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`, 8.7). It is now mitigated in the latest release and is assigned [CVE-2024-1451](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1451).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### User with "admin_group_members" permission can invite other groups to gain owner access

An issue has been discovered in GitLab EE affecting all versions starting from 16.5 before 16.7.6, all versions starting from 16.8 before 16.8.3, all versions starting from 16.9 before 16.9.1. When a user is assigned a custom role with admin_group_member permission, they may be able to make a group, other members or themselves Owners of that group, which may lead to privilege escalation. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:L`, 6.7). It is now mitigated in the latest release and is assigned [CVE-2023-6477](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6477).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS issue in the Codeowners reference extractor

An issue has been discovered in GitLab EE affecting all versions starting from 11.3 before 16.7.6, all versions starting from 16.8 before 16.8.3, all versions starting from 16.9 before 16.9.1. It was possible for an attacker to cause a client-side denial of service using malicious crafted content in the CODEOWNERS file.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5). It is now mitigated in the latest release and is assigned [CVE-2023-6736](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6736).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### LDAP user can reset password using secondary email and login using direct authentication

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.1 before 16.7.2, all versions starting from 16.8 before 16.8.2, all versions starting from 16.9 before 16.9.2. Under some specialized conditions, an LDAP user may be able to reset their password using their verified secondary email address and sign-in using direct authentication with the reset password, bypassing LDAP. This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:H/A:N`, 5.3). It is now mitigated in the latest release and is assigned [CVE-2024-1525](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1525).

This vulnerability was discovered internally by a GitLab team member, [Drew Blessing](https://gitlab.com/dblessing).


### Bypassing group ip restriction settings to access environment details of projects through Environments/Operations Dashboard

An issue has been discovered in GitLab EE affecting all versions starting from 12.0 to 16.7.6, all versions starting from 16.8 before 16.8.3, all versions starting from 16.9 before 16.9.1. This vulnerability allows for bypassing the 'group ip restriction' settings to access environment details of projects. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`, 4.3). It is now mitigated in the latest release and is assigned [CVE-2023-4895](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4895).

Thanks [albatraoz](https://hackerone.com/albatraoz) for reporting this vulnerability through our HackerOne bug bounty program.


### Users with the `Guest` role can change `Custom dashboard projects` settings for projects in the victim group

An issue has been discovered in GitLab EE affecting all versions starting from 16.4 before 16.7.6, all versions starting from 16.8 before 16.8.3, all versions starting from 16.9 before 16.9.1. Users with the `Guest` role can change `Custom dashboard projects` settings contrary to permissions. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N`, 4.3). It is now mitigated in the latest release and is assigned [CVE-2024-0861](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0861).

Thanks [them4les_l1r](https://hackerone.com/them4les_l1r) for reporting this vulnerability through our HackerOne bug bounty program.


### Group member with sub-maintainer role can change title of shared private deploy keys

An issue has been discovered in GitLab affecting all versions before 16.7.6, all versions starting from 16.8 before 16.8.3, all versions starting from 16.9 before 16.9.1. It was possible for group members with sub-maintainer role to change the title of privately accessible deploy keys associated with projects in the group.
This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:L/A:N`, 3.7). It is now mitigated in the latest release and is assigned [CVE-2023-3509](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3509).

Thanks [theluci](https://hackerone.com/theluci) for reporting this vulnerability through our HackerOne bug bounty program.


### Bypassing approvals of CODEOWNERS

An authorization bypass vulnerability was discovered in GitLab affecting versions 15.1 prior to 16.7.6, 16.8 prior to 16.8.3, and 16.9 prior to 16.9.1. A developer could bypass CODEOWNERS approvals by creating a merge conflict. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:N/I:L/A:N`, 3.0). It is now mitigated in the latest release and is assigned [CVE-2024-0410](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0410).

Thanks [ali_shehab](https://hackerone.com/ali_shehab) for reporting this vulnerability through our HackerOne bug bounty program.


## Non Security Patches

* [Invalidate markdown cache to clear up stored XSS](https://gitlab.com/gitlab-org/gitlab/-/issues/441094)

### 16.9.1

* [Merge branch 'ac-fix-16-9-0-changelog' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144758)
* [[Backport] Revert '437616_fix_changelog_tag_detection'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144759)
* [Backport Web IDE upgrade into 16.9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144956)
* [Fix deny_all_requests_except_allowed of AddressableUrlValidator](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144995)
* [Introduce back ci_pipeline_variables routing table FF](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144952)

### 16.8.3

* [Backport 'jc/fix-add-tree-entry' into 16-8-stable](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6674)
* [Allow creation of group-level custom-roles on self-managed instances](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144119)
* [Backport 'Fix stable cache for quick actions'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144217)
* [Fix X.509 commit signing for OpenSSL 3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144355)
* [Fix urlblocker validate calls with more options](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144349)

### 16.7.6

* [Backport jc/fix-add-tree-entry into 16-7-stable](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6675)
* [Fix X.509 commit signing for OpenSSL 3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144357)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
